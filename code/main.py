"""
Use rshell to upload code to RPI Nano
$ cd <code dir>
$ rshell
$ cp * /pyboard

start python's repl to get serial data
$ repl

do a soft restart of MCU to load code
$ <C-d>

exit REPL
$ <C-x>
"""

from machine import Pin, I2C, PWM, Timer
import time

# calculate time diffs for debouncing
import utime

# for interrupt debugging
import micropython
micropython.alloc_emergency_exception_buf(100)


import onewire, ds18x20             # DS18S20 NTC
from ssd1306 import SSD1306_I2C     # OLED display


PIN_SDA0 = 20
PIN_SCL0 = 21
PIN_LED_INTERNAL = 25
PIN_LED_STATUS = 16

PIN_RE_A = 13
PIN_RE_B = 14
PIN_RE_S = 15

PIN_TEMP = 28
PIN_HEATING = 2

DEFAULT_TARGET_TEMP = 30

OLED_LINE_HEIGHT = 13
OLED_X_PIXELS = 128
OLED_Y_PIXELS = 64

GRAPH_LEN = 14

# how many degrees before target temperature should PWM finetune kick in
PWM_TUNER_DEGREES = 1

# how many degrees (celcius) should rotary encoder increment per step
RE_INCREMENT = 0.1

# how often (ms) should the temperature be measured
TEMP_DELAY = 2000

PWM_MAX = 65025


class DeviceException(Exception):
    def __init__(self, message):
        super().__init__(message)
        # init i2c bus
        i2c = I2C(0,sda=Pin(PIN_SDA0), scl=Pin(PIN_SCL0), freq=400000)

        # init OLED
        oled = SSD1306_I2C(128, 64, i2c)

        print("ERROR:", message)

        oled.fill(0)
        oled.text("DeviceException", 0, 0)
        oled.text(str(message), 0, 1*OLED_LINE_HEIGHT)
        oled.show()


class State():
    def __init__(self, oled):
        self._oled = oled
        self.last_temp = None
        self.target_temp = DEFAULT_TARGET_TEMP
        self.status = "None"
        self.pwm = None
        self.epoch = time.time()

    def get_time_formatted(self, seconds):
        days = int(seconds / (60*60*24))
        rem = seconds % (60*60*24)

        hours = int(rem / (60*60))
        rem = seconds % (60*60)

        minutes = int(rem / (60))
        return "{0:02}:{1:02}:{2:02}".format(days, hours, minutes)

    def update_oled(self):
        temp_now = "{0:.1f}".format(round(self.last_temp, 1))
        temp_target = "{0:.1f}".format(round(self.target_temp, 1))
        spaces = " " * (16 - len(temp_now) - len(temp_target))

        self._oled.text("{0}{1}{2}".format(temp_now, spaces, temp_target), 0, 0*OLED_LINE_HEIGHT)
        self._oled.text("    " + self.get_time_formatted(time.time() - self.epoch), 0, 1*OLED_LINE_HEIGHT)
        self._oled.text(self.status, 0, 2*OLED_LINE_HEIGHT)


class RotaryEncoder():
    """ Baseclass to handle rotary encoders.
        Override the on_* methods to customise. """
    def __init__(self, pin_a, pin_b, pin_s):
        self.pin_a = Pin(pin_a, Pin.IN, Pin.PULL_UP)
        self.pin_b = Pin(pin_b, Pin.IN, Pin.PULL_UP)
        self.pin_s = Pin(pin_s, Pin.IN, Pin.PULL_UP)
        self.pin_a.irq(trigger=Pin.IRQ_RISING, handler=self.rotary_callback)
        self.pin_s.irq(trigger=Pin.IRQ_FALLING, handler=self.button_callback)

        # keep track of time between presses for debouncing
        self.last_button_press = None
        self.last_turn = None

        self.debounce_ms = 100

    def on_cw(self):
        print("clockwise")

    def on_ccw(self):
        print("counter clockwise")

    def on_button(self):
        print("button press")

    def check_debounce(self, t_last, debounce_ms):
        if t_last == None:
            return True

        if utime.ticks_diff(utime.ticks_ms(), t_last) > debounce_ms:
            return True

    def rotary_callback(self, pin):
        if not self.check_debounce(self.last_turn, self.debounce_ms):
            print("Waiting")
            return

        self.last_turn = utime.ticks_ms()

        if self.pin_b.value():
            self.on_ccw()
        else:
            self.on_cw()

    def button_callback(self, pin):
        if not self.check_debounce(self.last_button_press, self.debounce_ms):
            return

        self.last_button_press = utime.ticks_ms()
        self.on_button()


class TempSensor():
    def __init__(self, pin, delay=750):
        # init 1wire sensor
        pin_temp = machine.Pin(pin)
        self.sensor = ds18x20.DS18X20(onewire.OneWire(pin_temp))
        self._delay = delay

        try:
            self.device = self.sensor.scan()[0]
        except IndexError:
            print("Onewire devices found:", self.sensor.scan())
            raise DeviceException("No temperature sensor found!")

    def get_temp(self):
        self.sensor.convert_temp()
        time.sleep_ms(self._delay)
        return self.sensor.read_temp(self.device)


class REMenu(RotaryEncoder):
    def __init__(self, state, plot, oled, *args):
        RotaryEncoder.__init__(self, *args)
        self._s = state
        self._p = plot
        self._oled = oled

    def on_cw(self):
        print("clockwise")
        self._s.target_temp += RE_INCREMENT
        self._p.set_plot(y_min=None, y_max=self._s.target_temp)
        self._s.update_oled()
        self._oled.show()

    def on_ccw(self):
        print("counter clockwise")
        self._s.target_temp -= RE_INCREMENT
        self._p.set_plot(y_min=None, y_max=self._s.target_temp)
        self._s.update_oled()
        self._oled.show()

    def on_button(self):
        print("button press")


class HeaterHandler():
    def __init__(self, pin, status, pwm_tuner_degrees=5):
        self.s = status

        # init heating pin
        self.heater = PWM(Pin(pin, Pin.OUT))
        self.heater.freq(1000)
        self.heater.duty_u16(0)

        self.pwm_max = PWM_MAX

        # the difference in deg celcius and target temp from where we should
        # start scaling
        self.pwm_tuner_degrees = pwm_tuner_degrees

    def map_value(self, value, a_min, a_max, b_min, b_max):
        """ Map/scale one range to another """
        # Figure out how 'wide' each range is
        a_span = a_max - a_min
        b_span = b_max - b_min

        try:
            # Convert the left range into a 0-1 range (float)
            value_scaled = float(value - a_min) / float(a_span)
        except ZeroDivisionError:
            return  0

        # Convert the 0-1 range into a value in the right range.
        return b_min + (value_scaled * b_span)

    def get_graph(self, value, high, length, l_chr='.', r_chr=' '):
        l_size = int((value / high) * length)
        r_size = length - l_size
        graph = (l_size * l_chr) + (r_size * r_chr)
        #print("[" + graph + "]")
        return "[" + graph + "]"

    def check_temp(self):
        # start scaling PWM at this temp
        finetune_tresh = self.s.target_temp - self.pwm_tuner_degrees

        # if we're below treshold, blast heating at full power
        if self.s.last_temp < finetune_tresh:
            self.heater.duty_u16(PWM_MAX)
            self.s.status = self.get_graph(PWM_MAX, PWM_MAX, GRAPH_LEN)
            self.s.pwm = PWM_MAX

        else:
            # we're above treshold so we're going to scale our PWM
            # proportional to the difference between current temp
            # and target temp
            duty = self.map_value(self.s.last_temp,
                                  finetune_tresh,
                                  self.s.target_temp,
                                  PWM_MAX,
                                  0)
            if duty < 0:
                duty = 0

            duty = int(duty)

            self.heater.duty_u16(duty)
            self.s.status = self.get_graph(duty, PWM_MAX, GRAPH_LEN)
            self.s.pwm = duty


class Plot():
    """ Create a plot and draw on oled """
    def __init__(self, oled, x_size=127, y_size=63, y_offset=0):
        # size in pixels
        self._x_size = x_size
        self._y_size = y_size

        # offset from top in pixels
        self._y_offset = y_offset

        # oled object
        self._oled = oled

        # lines to display
        self._lines = [] * int(OLED_Y_PIXELS / OLED_LINE_HEIGHT)

        self._datapoints_per_pixel = 5

        self._points = []
        self._tmp_bin = []

    def map_value(self, value, a_min, a_max, b_min, b_max):
        """ Map/scale one range to another """
        # Figure out how 'wide' each range is
        a_span = a_max - a_min
        b_span = b_max - b_min

        try:
            # Convert the left range into a 0-1 range (float)
            value_scaled = float(value - a_min) / float(a_span)
        except ZeroDivisionError:
            return  0

        # Convert the 0-1 range into a value in the right range.
        return b_min + (value_scaled * b_span)

    def add(self, y):
        # when point completes bin
        if len(self._tmp_bin) == self._datapoints_per_pixel-1:
            self._points.append(sum(self._tmp_bin) / len(self._tmp_bin))
            self._tmp_bin = []

        # when point is not the last point in the bin
        elif len(self._tmp_bin) < self._datapoints_per_pixel:
            self._tmp_bin.append(y)

        # make sure we don't keep all data, that will lead to OOM
        self._points = self._points[-self._x_size:]
        
    def set_plot(self, y_min=None, y_max=None):
        # clear oled display
        self._oled.fill(0)

        # check if max/min is out of bounds or autoranging is enabled
        if y_max == None or max(self._points, default=30) > y_max:
            y_max = max(self._points, default=30)

        if y_min == None or min(self._points, default=0) < y_min:
            y_min = min(self._points, default=0)

        # keep track of last y value for antialiassing
        last_y = None

        # create bins from datapoints
        for x, y in enumerate(self._points):

            # map temperature to pixels
            mapped_y = self.map_value(y, y_min, y_max, self._y_size, 0)
            mapped_y = int(mapped_y)

            if last_y == None:
                last_y = mapped_y
                continue

            # fill in the vertical gaps
            if mapped_y < last_y:
                for a in range(mapped_y, last_y+1):
                    self._oled.pixel(x, a + self._y_offset, 1)
            elif mapped_y >= last_y:
                for a in range(last_y, mapped_y+1):
                    self._oled.pixel(x, a + self._y_offset, 1)

            last_y = mapped_y


def run():
    # init i2c bus
    i2c = I2C(0,sda=Pin(PIN_SDA0), scl=Pin(PIN_SCL0), freq=400000)

    # init status led
    led = Pin(PIN_LED_INTERNAL, Pin.OUT)
    led_status = Pin(PIN_LED_STATUS, Pin.OUT)
    led_status.off()

    # init OLED
    oled = SSD1306_I2C(128, 64, i2c)

    # keep track of application state in object
    s = State(oled)

    ts = TempSensor(PIN_TEMP, delay=TEMP_DELAY)

    # a nice temperature plot
    y_offset = 3*OLED_LINE_HEIGHT
    graph_height = OLED_Y_PIXELS-1-y_offset
    p = Plot(oled, x_size=OLED_X_PIXELS-1, y_size=graph_height, y_offset=y_offset)

    # init rotary encoder
    re = REMenu(s, p, oled, PIN_RE_A, PIN_RE_B, PIN_RE_S)

    heating = HeaterHandler(PIN_HEATING, s, pwm_tuner_degrees=PWM_TUNER_DEGREES)

    while True:
        # this will block the loop (TEMP_DELAY)
        s.last_temp = ts.get_temp()

        # handle heater
        heating.check_temp()

        # clear display
        oled.fill(0)

        # set temp graph on display
        p.add(s.last_temp)
        p.set_plot(y_min=None, y_max=s.target_temp)

        s.update_oled()

        oled.show()

# run mainloop, catch all exceptions and run some custom code
try:
    run()
except Exception as e:
    raise DeviceException(e)
